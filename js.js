var rect_main_slider = document.querySelector('.slider').getBoundingClientRect();
var thumb_slider = document.querySelector('.thumb');
var is_active_slider = false;

thumb_slider.onmousedown = (event) => {
    event.preventDefault(); // может быть выделение, лучше отменить
    is_active_slider = true;
}

function move_slider_at(x) {
    thumb_slider.style.left = x + 'px';
}

document.onmousemove = (event) => {
    if (is_active_slider) {
        let x = event.pageX - rect_main_slider.left;

        if (x < 0)
            x = 0;
        else if (x >= rect_main_slider.width - thumb_slider.offsetWidth / 2)
            x = rect_main_slider.width - thumb_slider.offsetWidth;

        move_slider_at(x);
    }
}

document.onmouseup = (event) => {
    if (is_active_slider)
        is_active_slider = false;
}

thumb_slider.ondragstart = () => {
    return false; // может быть стандартный перенос браузера, нам нужен свой, поэтому отменяем
}